import Vue from 'vue'
import App from './App.vue'
import vuetify from "./plugins/vuetify";
import {store} from "./store/store";
import router from "./router/index"

new Vue({
  el: '#app',
  vuetify,
  store,
  router,
  render: h => h(App)
})
